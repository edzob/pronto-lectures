---
title: "Personen relevant voor Pronto"
linkTitle: "Personen"
weight: 2
description: >
  Wie speelden een rol in de creatie van Pronto
---


# Pronto Grondleggers
Bert Noorman dient gezien te worden als de grondlegger
van Pronto en de eigenaar van het gedachtengoed.
Bert heeft nauw samengewerkt met Adosh van der Heijden
en Michael Oosterhout in de periode 2006 tot 2011 om samen
Pronto uit te breiden en te verbeteren met hun gezamelijke 
kennis, ervaringen en kunde. 
Zie de Gesciedenis pagina voor meer informatie. 

| Name                   | Current / Profile   | Discription  
|-------------------     |-----------------    |----------------- 
| Bert Noorman           | ...                 | Geestelijk vader van Pronto   
| Adosh van der Heijden  | ...                 | ...   
| Michael Oosterhout     | ...                 | ...          

# Pronto Kernteam
In de periode van 2006 tot 2011 werd de ontwikkeling van
Pronto actief ondersteund vanuit de business Development 
binnen Sogeti Nederland.
In de periode na 2011 is de verantwoordelijkheid om Pronto
actueel en levend te houden rond gegeven binnen de populatie van 
professionals.

| Name                  | Current / Profile   | Discription  
|-------------------    |-----------------    |----------------- 
| Pieter Veefkind       | ...                 | ...   


# Pronto-Lectures secretarissen
Edzo Botjes en Ton Eusterbrock hebben zich beschikbaar gesteld
om alle kennis rondom Pronto vanuit gesproken woord te vertalen
naar deze website. 
Het doel is om de kennis en ervaring die nu opgeslagen ligt
in de hoofden van de grondleggers te ontsluiten voor het brede publiek.
Zodat professionals, fans en onderzoekers deze informatie kunnen vinden 
en raadplegen via het internet. 

Het opschrijven en conserveren van de kennis voor komende generaties.

| Name                  | Current / Profile   | Discription  
|-------------------    |-----------------    |----------------- 
| Edzo Botjes           | ...                 | ...   
| Ton Eusterbrock       | ...                 | ...   

