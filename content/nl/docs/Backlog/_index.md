---
title: "Backlog"
linkTitle: "Backlog"
weight: 100
description: >
  Mogelijk invalshoeken om over Pronto te praten
---

1. Pronto modellen als (ver)bouwtekeningen; bouwtekeningen vs “artist impressions”
1. Veranderen, veranderanalyse en impact analyse; what-if; elke verandering is een business verandering!
1. Rood, groen, blauw / data, informatie, intentie / systeem-, info-, business analist
1. Architectuur en Business Analyse, Architectuur en Pronto, Business Analyse en Pronto
1. Pronto en Demo; historie & achtergrond, doelen & opbouw, verschillen (& overeenkomsten)
1. Klantreizen / customer journeys, (customer) life-cycles en “kontaktmomenten”
1. Management vs operatie; enterprise model; bedrijfsvoering en control structuur; stuurmiddel vs operationele ondersteuning
1. Knelpunten vinden, optimaliseren, verbeteren (Pronto en Lean / 6sigma)
1. Relatie met andere relevante modellen en frameworks
1. Verantwoordelijkheden; accountability, eigenaarschap; (bedrijfs)domein, super-domeinen, subdomeinen; verschil commerciële onderneming en overheid
1. Business ⬄ IT (bridging the gap); Pronto = business met een ‘bridge’ naar IT
1. “Elke verandering is een business verandering!” en Pronto is ‘rood’
1. Informatiebehoeften, informatiestromen, informatievoorziening / IT; relatie met bedrijfs-voering; matchen van behoeften, CRUD-matrix; SPOT, eigenaarschap
1. Informatie als basis voor analyses; informatie architectuur; (master) data management 
1. Kwaliteitszorg (en certificering); meten en sturen, KPI’s en Pronto
1. Pronto en (agile) ontwikkeling; “projecten” anders bekijken; “niveaus” in processen
1. Ketendenken; waardeketens (value chains), transactieketens, procesketens; ketenverantwoordelijkheid, keten management; keten optimalisatie
1. Producten en diensten; product/diensten catalogi; P&D architectuur; productopties/keuzes
1. Verander cyclus; richten, inrichten, verrichten vs Pronto
1. Herkenbare patronen in enterprise modellen; hergebruik (transactie!); verschillende patronen voor (commerciële) bedrijven /overheid / projecten; zaakgericht werken; patronen voor branches; bibliotheek onderhouden
1. Proces management / proces modellering / proces analyse en Pronto; bedrijfsprocessen, werkprocessen (met procedures, werkinstructies, flowcharts, enz.)
1. Wat betekent accountability? Wat betekent eigenaarschap? Waarom is dat essentieel?
1. Tooling voor Pronto modellen; te stellen eisen, mapping van concepten
1. Pronto concepten: product/dienst, (bedrijfs)transactie, (bedrijfs)proces, (bedrijfs)keten, (hoofd)activiteiten, informatiebehoeften en -stromen (in en uit), informatie objecten;
zie ook de aparte begrippenlijst