---
title: "Generieke analysevragen"
linkTitle: "Analysevragen"
weight: 50
description: >
  Welke stappen leiden tot een gedegen analyse
---


De eerste vraag moet altijd zijn:
1. Wat is de doelstelling en de scope (met achtergrond, randvoorwaarden, etc.) van de analyse en vanuit welk(e) domein(en) bekijken we de onderneming (bedrijf, organisatie)?

Van daaruit gaat het in hoofdlijnen om de volgende vragen (stappen):
1. Wat zijn de relevante producten en diensten, in het kader van de gekozen scope en vanuit klantperspectief? (Anders gezegd: wat is de bestaansreden van het domein?)

1. Welke klanttransacties (= klantketens) zijn bij die producten en diensten relevant?

1. Wat gebeurt er in het bedrijfsproces van een transactie (hoofdactiviteiten)?

1. Hoe zien de ketens achter die klanttransacties er uit? Bij welke hoofdactiviteiten van de klanttransactie is een bijdrage van andere domeinen nodig / welke domeinen moeten (via transacties of op een andere manier) welke bijdrage leveren in de keten?

1. ...
    1. Wat is de informatiebehoefte van elke transactie / van het bedrijfsproces? (Hoe) wordt daarin voorzien?
    1. Welke informatie moet de transactie (het proces) vastleggen voor gebruik door andere transacties (processen)?

1. Is de totale bedrijfsinformatiebehoefte (gegeven de gekozen scope) belegd in informatie-objecten (waarvan de eigenaren bekend zijn)? Is deze set consistent en zonder doublures?
En is waar nodig in alle elementaire beheertaken voorzien (creëren, wijzigen, verwijderen en lezen)?