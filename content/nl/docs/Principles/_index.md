---
title: "Basisprincipes"
linkTitle: "Basisprincipes"
weight: 40
description: >
  Dit zijn de 5 Basisprincipes voor werken met Pronto
---


## Principe 1

Transacties, processen, ketens en infrmatiestromen geven vorm aan samenwerking. Onderscheidt hierbij samenwerking tussen domeinen van samenwerking tussen (groepen) mensen. Het laatste gaat over “dingen doen” (responsabilities), het eerste over “zaken doen” (accountabilities).
Als de samenwerking tussen domeinen duidelijk is (eigen accountability, dus zekere mate van autonomie) kan de samenwerking binnen domeinen relatief makkelijk vorm worden gegeven (en naar behoefte van medewerkers en eigenaar!).
Pronto gaat over samenwerking tussen domeinen (en over zaken doen en accountabilities).

## Principe 2

Modelleer alleen wat zinnig en nuttig is: de essentie van samenwerking.
De essentie is een compleet overzicht van de bedrijfsvoering over de verschillende domeinen heen (en binnen de gekozen scope).
Dat wil zeggen van alle transactie(keten)s, bedrijfsprocessen en alle benodigde informatiestromen en -objecten en als een integraal model!
Houdt het model zo compact mogelijk; dat geeft het beste overzicht, betere acceptatie en vereenvoudigt analyses en onderhoudswerk. Waar onderdelen* gecombineerd kunnen worden: doen (tip bij analyses: eerst gescheiden analyseren, dan waar mogelijk en zinnig combineren).
     *  onderdelen = producten / diensten, transacties, processen, hoofdactiviteiten, informatiestromen, etc. 
      Niet te combineren (evt. wel te dupliceren): dingen van verschillende eigenaren!

## Principe 3

Mate van detaillering speelt vooral bij de uitwerking van processen in hoofdactiviteiten (zie ook de omschrijving van ‘hoofdactiviteit’). Hier gelden 3 belangrijke criteria:
 - zijn alle punten waarop de inbreng van een ander domein nodig is naar voren gekomen?
 - zijn alle informatiebehoeften bekeken en geïdentificeerd (in en uit)?
 - kan (wil) de eigenaar zich herkennen in de beschrijving (acceptatie)?

## Principe 4

Modelleer elk bedrijfsproces vanuit de 5 fasen van een transactie.  Gebruik waar mogelijk herkenbare patronen (voor begrip, efficiëntie, herkenbaarheid, hergebruik, overdraagbaarheid, enz.). 
Voorbeelden:
  Basis voor bedrijfsvoering / een bedrijfsmodel: er zijn altijd klanten, producten/diensten en “relaties daartussen” (CZ)
  Modelleren van betalingen: voorwaardelijk voor levering, achteraf, gratis, ……
  Productkeuzes: liever product/dienst met keuze-opties dan alles opsplitsen en aparte ketens (KPN)
  Van product/dienst naar klanttransacties: relatie/overeenkomst, uitnutting, klantenservice (checklist)
  Omgaan meet offerte, garantie, bedenktijd, etc. in een transactie (vgl ook DEMO)

## Principe 5

In de Pronto modellen komt specifieke informatie maar op 1 plek (in één specifiek informatieobject van de onderneming) voor. In de realisatie kunnen om allerlei redenen meerdere kopieën op verschillende locaties nodig zijn.
