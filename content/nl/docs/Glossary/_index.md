---
title: "Termen"
linkTitle: "Termen"
weight: 30
description: >
  Definities van de termen die binnen Pronto worden gebruikt
---

## Pronto  

Staat voor **PR**oces **ONTO**logie (credits: Michael Oosterhout, 2007); de naam is afgeleid van de titel van het boek ‘Enterprise Onthology’ van Jan Dietz.

Ontologie, ook wel “zijnsleer” genoemd, is een begrip uit de filosofie, vanuit de centrale vraag: “wat is (de essentie van het) zijn?”

## Prontologie 

Ofwel “De leer van het snelle begrip” (credits: Hans Mulder, 2008).
Dat slaat op het doel achter Pronto: 
het snel kunnen begrijpen van (de essentie van) een onderneming 
(bedrijf, organisatie), 
om op basis daarvan beter te kunnen (be)sturen, besluiten en veranderen

## Accountability  

Pronto gaat primair over accountability (en niet over responsibility). 

Accountability is gekoppeld aan eindverantwoordelijkheid en eigenaarschap.

Verantwoordelijkheid vs. accountability gaat over inspanning vs resultaat of anders gezegd over het uitvoeren van taken vs de eindverantwoordelijkheid voor een goede uitvoering en een goed resultaat. Accountability omvat maatregelen vooraf (bv transacties en processen inrichten), tijdens (bv monitoring en rapportages) en achteraf (verantwoording en aanspreekbaarheid).
Accountability gaat over regels maken en zaken doen, responsibility over regels volgen en dingen doen. 

## (Bedrijfs)domein  

Een organisatorische eenheid, met een eigen ‘accountability’ (zie RACI). Accountability of eindverantwoordelijkheid (voor iets) is een centraal begrip in Pronto en wordt direct gekoppeld aan ‘ketens’ en aan eigenaarschap van o.a. transacties, processen en informatie, zie hieronder)

## Super-/sub-domein 

Een hiërarchisch boven- dan wel onderliggend domein. 
Een superdomein kan (spel)regels opleggen aan een subdomein, 
waarmee de accountability van het subdomein wordt ingekaderd 
(soms met maar vaker zonder aparte transactie)

```mermaid
graph TB;
    A((Super-domein))-- regels -->B((Sub-domein))
```


## Product / Dienst  

Met een product (fysiek) of dienst (niet fysiek) wordt een klantbehoefte bevredigd. De uiteindelijke bestaansreden van een domein is, dat er klanten zijn die de aangeboden producten of diensten willen afnemen (of moeten afnemen, denk aan de overheid). Zie verder onder ‘(klant)transactie’.

## Bedrijfsvoering 

De essentiële zaken in de manier waarop een onderneming (bedrijf, organisatie) haar producten en/of diensten levert aan haar klanten.
Essentieel is in deze context datgene wat met en door Pronto wordt gemodelleerd: de werking en constructie van een bedrijf (onderneming).

## (Bedrijfs)transactie  

Een transactie (eigenlijk: transactietype) is een bouwsteen van de bedrijfsvoering en een middel om het leveren van een product of dienst (aan een klant) vorm te geven. Een transactie beschrijft de samenwerking tussen 2 domeinen in termen van het op een gestructureerde manier aangaan en nakomen van wederzijdse verplichtingen (de klant heeft een eigen accountability en is in dit kader dus ook als een domein te beschouwen).

**Belangrijke concepten:**
Een transactie heeft als basis een klant ⬄ leverancier model en is gebaseerd op het concept van prestatie / tegenprestatie.
De “Pronto transactie” is een afgeleide van het begrip ‘transactie’ in het eerder genoemde boek van Jan Dietz – zie onder ‘transactiefasen’
Voor het leveren van een product of dienst bestaan in het algemeen  meerdere klanttransacties naast elkaar (basispatroon, zie onder ‘klanttransactie’)
elke transactie is een potentiële “eenheid van hergebruik” in verschillende ‘bedrijfsketens’ (zie ook onder ‘primair / secundair’).

## Klant 

Het begrip ‘klant’ is gekoppeld  aan het begrip transactie 
(“de vragende kant”). 
Een klant heeft een behoefte te bevredigen 
(en de leverancier voorziet daar in).
Een klant kan extern zijn (eindklant), 
maar ook intern (denk aan een transactie tussen 2 bedrijfsdomeinen). 
In Pronto wordt met ‘ klant’ meestal de eindklant bedoeld 
(zie ook ‘keten’).

## (Bedrijfs)proces  

Een bedrijfsproces is de procesmatige uitwerking van een transactie 
(1 proces = 1 transactie). 
Elk bedrijfsproces is (daarom) onderdeel van “de essentie”. 
Bij afspraak is de leverancier eigenaar van de transactie 
en van het bijbehorendee bedrijfsproces

## Werkproces  

Een werkproces of operationeel proces 
(denk b.v. ook aan een flowchart) 
is een meer gedetailleerde uitwerking van een 
(deel van een) bedrijfsproces 
en geen onderdeel van de “essentie” 
(en dus ook niet van Pronto modellering).

## Transactieketen 

Bij de realisatie van een klantbehoefte 
spelen meestal meerdere domeinen een rol. 
Elke samenwerking tussen 2 domeinen heeft als basis een transactie. 
Er bestaat dan dus een keten van transacties, 
waarmee een klantbehoefte wordt ingevuld. Vergelijkbare termen: keten, waardeketen, (bedrijfs)keten, 
(bedrijfs)procesketen (afhankelijk van de gekozen focus).



## Klanttransactie 

Een klanttransactie is een transactie, 
die richting klant invulling geeft aan een te leveren product of dienst. 
Het gaat hierbij dus (vanuit het leverende domein gezien) 
altijd om de ‘eindklant’ (ook weer een relatief begrip!). 
Een klanttransactie is vanuit het domein gezien 
het begin van een bedrijfsketen en definieert die keten ook.
Een product of dienst wordt in het algemeen vertaald 
in meerdere klanttransacties.

Als het product ‘schadeverzekering’ is zijn er klanttransacties 
(en dus ook ketens) 
nodig voor het afsluiten / wijzigen / beëindigen van een verzekering, 
het claimen van een schade en voor het afhandelen van vragen of klachten.

Een product bestaat vaak uit meerdere deelproducten of deeldiensten. Als het product van een dealer een auto is, is er normaal gesproken ook een dienst “onderhoud” en eventueel ook een “pechdienst” aan gekoppeld. Hiervoor bestaan dan ook aparte klanttransacties en ketens.

## Transactiefasen 

Een transactie kent 5 stappen (fasen) in een vaste volgorde (bron: ‘Enterprise Onthology’ van Jan Dietz, 2006). 
De verdere uitwerking/invulling van deze fasen vindt plaats in het achterliggende bedrijfsproces – zie onder ‘(hoofd)activiteiten’.

De 5 fasen zijn: Verzoek, Belofte, Uitvoering, Oplevering en Acceptatie.

```mermaid
sequenceDiagram
    participant K as Klant
    participant L as Leverancier
    K-->>L: Verzoek
    L-->>K: Belofte
    activate L
      Note right of L: Uitvoering
    L-->>K: Oplevering
        deactivate L
    K-->>L: Acceptatie
```


## (Hoofd)activiteit 
Binnen elke fase van een transactie 
moeten in het bedrijfsproces de hoofdactiviteiten worden benoemd. 
De diepgang daarvan wordt bepaald op basis van 3 overwegingen:
1. Overzicht en inzicht: 
wat gebeurt er in dit proces en wat (dus) niet 
(niveau: samenwerking tussen accountable domeinen);

1. Bij de uitvoering van welke hoofdactiviteiten zijn andere domeinen nodig (extra schakels in de keten identificeren) en

1. Wat is de informatie behoefte van het proces:
    1. Wat heeft dit proces nodig aan bedrijfsinformatie (meestal vanuit andere processen) en 
    1. Welke informatie hebben andere processen nodig (die uit dit proces moet komen)?

Over de detaillering kan nog wel eens discussie ontstaan. 
Dan gelden de 3 overwegingen, 
daarna basisprincipe 2 (hieronder) en 
in laatste instantie beslist de eigenaar 
(vanwege acceptatie en accountability).

## (Bedrijfs)informatie  

Bedrijfsinformatie is die informatie, 
die alle bedrijfsprocessen samen nodig hebben om hun werk te kunnen doen 
en die door of aan andere bedrijfs-processen 
(ook van externe partijen) geleverd moet worden. 
Daaronder vallen bv ook het gebruik van informatie uit de GBA of rapportages, intern of extern).

In de Pronto analyses staat centraal, dat in de informatiebehoeften van alle bedrijfsprocessen kan worden voorzien (in en uit) en dat duidelijk is wat de bron van de informatie is (die kan dus ook extern zijn).

## Informatiestroom  

In de Pronto modellen wordt elke informatiestroom gerepresenteerd door een “pijl” van informatieobject naar hoofdactiviteit 
(“inkomend”, waarmee de informatiebehoefte van een bedrijfsproces duidelijk wordt) 

```mermaid
graph LR;
    A[Informatie Object]-->B[Hoofdactiviteit];
```
en andersom van activiteiten naar objecten (“uitgaand”) 
waarmee de te leveren informatie voor andere processen duidelijk wordt.

```mermaid
graph RL;
    A[Hoofdactiviteit]-->B[Informatie Object];
```

Het gaat bij een informatiestroom om de specifiek benodigde informatie 
(dus b.v. NAW-gegevens i.p.v. klantinformatie; 
geen hele objecten, maar specifieke velden) 
- zie ook: informatieservice.

## Informatieobject  

Een informatieobject is een verzameling bedrijfsinformatie, 
die logisch gezien bij elkaar hoort 
(b.v. alle informatie over klanten) 
en die één eigenaar heeft (de eigenaar is vaak, maar niet noodzakelijk, het domein dat de informatie produceert of vastlegt).

Alle informatieobjecten samen worden inhoudelijk gedefinieerd 
door de optelsom te maken van alle “inkomende” informatiestromen 
(= de totale informatiebehoefte van de onderneming).
Voor elk informatieobject moet vervolgens worden nagegaan:
a) of alle informatie daadwerkelijk gebaseerd is op een behoefte 
(“lezen”) en
b) of er voor alle informatievelden voldoende beheermogelijkheden 
(lees: bedrijfsprocessen) zijn om die informatie te creëren, 
te wijzigen en indien nodig te verwijderen (en zo nee, of dat ok is).

| CRUD|  Nederlands | Engels
|---- |---- |-----
|**C**| Creëren | Create
|**R**| Lezen | Read
|**U**| Aanpassen| Update
|**D**| Verwijderen | Delete

De term CRUD beschrijft de vier basis operaties die op informatie
uitgevoerd kunnen worden. 
Op de engelse 
[wikipedia](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete) 
 staan verschillende voorbeelden
van de technische vertaling van deze logische operaties.

## Informatieservice 

Dit  gaat over de doorvertaling naar IT (informatievoorziening i.p.v. informatiebehoeften) en hoort daarom niet bij de Pronto-modellen!

Informatie services zijn de implementatie van informatiestromen. 
Het is de “IT interface” voor het gebruik van / de toegang tot informatie uit informatie-objecten (objecten), 
dus voor het toevoegen, wijzigen en verwijderen (schrijven) 
of ophalen (lezen) van specifieke informatie uit een specifiek object.

De informatiestromen uit de Pronto modellen zijn een heel goede basis voor het inventariseren van de IT-requirements!