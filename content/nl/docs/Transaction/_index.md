---
title: "Transactie"
linkTitle: "Transactie"
weight: 60
description: >
  Wat is een transactie?
---


The collection of transaction patterns are not a model of reality
someone has invented or created. 
The transaction patterns are the reality.

When people interact then this interaction is always in the form
of a transaction pattern.
The pattern contains the five steps of:
1. Issue request.
1. Promise to the request.
1. The production of the promise.
1. The delivery of the produced outcome as manifestation of the promise.
1. The Acceptance of the delivered product.


```mermaid
sequenceDiagram
    participant K as Klant
    participant L as Leverancier
    K-->>L: Verzoek
    L-->>K: Belofte
    activate L
      Note right of L: Uitvoering
    L-->>K: Oplevering
        deactivate L
    K-->>L: Acceptatie
```

A person that works at a conveyor belt, has no part in the transaction pattern. 
In this situation this person is part of the operational execution (implementation). This person might be responsible for the execution of the job but not accountable.

A transaction pattern is always between two persons that are equal toward each other. The two actors in the pattern should both be accountable to be part of the transactions in scope of Pronto as part of the business essence. 

**The rule of thumb is: **
- An actor who is accountable determines the rules.
- An actor who is responsible follows the rules.

{{% pageinfo %}}
Pronto provides insight in who is accountable for what
and the transaction pattern between the accountable actors.
{{% /pageinfo %}}