---
title: "Pronto Documentatie"
linkTitle: "Documentatie"
weight: 1
menu:
  main:
    weight: 10
---

Welkom bij de Pronto Lectures.
Op deze pagina vindt u niet alleen
documentatie van Pronto maar in de toekomst
ook opnames van mini-colleges van
de bedenkers van Pronto.

## Business Essetie
1. Pronto focussed zich op de esssentie van een organisatie.
1. De essentie van een organisatie zijn de transacties tussen de personen
die accountable zijn.
1. Niet tot de essentie van een organisatie behoren de implementatie
in de vorm van rollen, delegatie, (bedrijfs) process definitie etc.