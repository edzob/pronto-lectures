---
title: "Glossary"
linkTitle: "Glossary"
weight: 30
description: >
  Glossary of terms used in Pronto
---

## Pronto

Pronto is an acronym for **PR**ocess **ONTO**logie 
(credits: Michael Oosterhout, 2007).

The name Pronto is a derivative of the title 
of the book 
"Enterprise Ontology" 
([WorldCat](http://www.worldcat.org/oclc/940080121),
[Google Books](https://books.google.nl/books?id=jg\_CXuVjJyIC), 
[GoodReads](https://www.goodreads.com/book/show/1243664.Enterprise_Ontology))
written in 2006 
by 
[Jan Dietz](https://en.wikipedia.org/wiki/Jan_Dietz).


## Prontology
*NL: Prontologie*

Prontology is a combination of Pronto and Ontology 
(credits: Hans Mulder, 2008).

The reasoning behind this combination is the goal of Pronto.
The goal of Pronto is to quickly comprehend 
the essence of an Enterprise.
An Enterprise can be a company or an organization.

Knowing the essence of an Enterprise 
should improve the 
change, decision and governance of this Enterprise.

## Ontology

Ontology is a term originating from the philosophy 
([Wikipedia](https://en.wikipedia.org/wiki/Ontology),
[Stanford.Plato](https://plato.stanford.edu/entries/logic-ontology/#Ont)).

The [Webster](https://www.merriam-webster.com/dictionary/ontology) dictionary defines ontology as: 
"a branch of metaphysics concerned with the nature and relations of being"

The central question of ontology is: What is the essence of being?

##  (Business) Domain
*NL: (Bedrijfs)domein*

A business domain is the organizational unit 
that has her own accountability.

Accountability, also known as end-responsibility, is pivotal in Pronto.
Accountability is directly linked to chains and 
to the ownership of transactions, processes and information (etc).

## Accountability

Accountability is the primary concern in Pronto. 
Pronto does not focus on responsibility.

Accountability is connected with end-responsibility and ownership.

Responsibility vs. accountability is about the difference
between effort and result.
The difference between responsible and accountable is
the difference between who executes the task,
and who is having the end-responsibility
for the correct execution and the quality of the result.

Accountability covers the moments 
before (e.g. transactions and implement processes),
during (e.g. monitoring and reporting) and
after (take responsibility and accountability).

Who is accountable defines the rules and can do business.
Who is responsible follows the rules and executes actions.

## Super-/ Sub- Domain

*NL: Super-/sub-domein*

A hierarchical positioning above or under another business domain.
A Super-Domain is able to define the rules a sub-domain needs to adhere to.
The accountability of a domain is limited by the rules defined by the super-domain.
The limitation of accountability can be the result of a transaction.

```mermaid
graph TB;
    A((Super-domain))-- rules -->B((Sub-domain))
```

## Product/ Service
*NL: Product / Dienst*

The (physical) product or (non-physical) service satisfies the need of the
customer.
The reason of existence (purpose) of a domain
is to have a value proposition (produces/ services) 
that is consumed by customers. 
This can be consummation by persuasion (free market)
or via mandatory consummation (for example from the government).


## Business Operations
*NL: Bedrijfsvoering*

The essential activities and ways how an enterprise produces
and delivers their products/ services to their customers.

Essential is here used in the context of what is discovered 
and modeled 
by using Pronto.

## (Business) Transaction
*NL: (Bedrijfs)transactie*

A transaction (type)
is a building block of the business operation.
A transaction is a tool to visualize (and design)
the supply of a product or services to a customer.

A transaction describes in a structured way 
the collaboration between 
two domains and the mutual agreed commitments.

The customer is accountable and therefore is also a domain in itself.


### Important Concepts

The base premise for the transaction is the 
customer - supplier model. 
This model is closely linked to the concept of providing a service or product and receiving compensation in return.

The transactions used in Pronto are a derivative from the
transactions described by Jan Dietz in 2006 in his book
Enterprise Ontology.

Delivering a product or services consists out of 
multiple customer transaction in co-existence. 
Every transaction is a possible "unit of re-use"
for other business chains. 

##  Customer
*NL: Klant*

The customer is the requesting party (actor) in the business transaction.
A customer has a need and the supplier has the ability to satisfy this need.

A customer outside of the Enterprise is identified as the end-customer. 
A customer can also be from inside of the Enterprise as for example the requesting party in the transaction between two business domains. 

In Pronto by default the customer is the end-customer.
See also "Business Chain".

## (Business) Process 
*NL: (Bedrijfs)proces*

A Business Process is the transaction 
translated into one business process.
One transaction leads to one and only one business process.
Every business process is therefore part of the business essence.
The supplier is owner of the transaction and therefore 
the supplier is the owner of the business process.



## Work Process
*NL: Werkproces*

A work process is also called an operational process.
is a more detailed design of (part of) 
the business process.

An example of a more detailed design is for example a flow-chart.

A detailed design is not part of the essence, therefor a 
the work process is not part of Pronto and Pronto models.

## (Business) chain
*NL: (Bedrijfs)keten*

To satisfy the customers need it is common for multiple domains to 
be involved. 
Domains can (only) collaborate via transactions between the domains.
To satisfy the customers' needs a chain of transactions is involved.

Synonyms for business chain are: value chain, transaction chain, 
process chain. 



## Customer Transaction
*NL: Klanttransactie*

A customer transaction is in Pronto synonym to a transaction.
A customer transaction is the transaction that delivers the
product/ service to the customer. 

One product usually constitutes out of multiple customer transactions.

For example for the product "insurance" the involved
customer transactions are at least the following six: the start, change 
and end of the insurance, claims, support and complaints.

A product is usually composed of multiple products.
For example Let a car be the product of a car dealership,
then it is not uncommon to have a service called maintenance,
and a service called road assistance. 

These services each have their own customer transactions and chains.

## Transaction Phases
*NL: Transactiefasen*

A transaction consists of five (5) transnational phases.
These phases have a specific order.
These phases were first identified by 
[Jan Dietz](https://en.wikipedia.org/wiki/Jan_Dietz)
in his book "Enterprise Ontology"
([WorldCat](http://www.worldcat.org/oclc/940080121),
[Google Books](https://books.google.nl/books?id=jg\_CXuVjJyIC), 
[GoodReads](https://www.goodreads.com/book/show/1243664.Enterprise_Ontology)).

1. Request
1. Promise
1. Produces
1. Deliver
1. Accept

```mermaid
sequenceDiagram
    participant K as Customer
    participant L as Supplier
    K-->>L: Request
    L-->>K: Promise
    activate L
      Note right of L: Produces
    L-->>K: Deliver
    deactivate L
    K-->>L: Accept
```

See (Main) Activity for the description of the five transnational phases.


## (Main) Activity
*NL: (Hoofd)activiteit*

During every phase of the business transaction
it is essential to identify the main activity
in the business process.

The level of detail of the activity description 
is based on one of the following three considerations.
1. provide overview and insight. 
What is happening in the process and what is not.
Level: collaboration between domains that are accountable.

1. Which main activities rely on other domains?
Level: identify extra shackles in the chain.

1. What is the information needed for the process?
   1. Which business information is needed *by* the process?
   1. Which business information is needed *from* the process by other processes?

Determination of the level of detail that is needed,
is up for discussion. 
To guide the discussion it best 
- to start with the three considerations,
- followed by the two basic principles (see below) and
- when there is no (clear) conclusion in the end the owner of the transaction/ process can decide. 
The owner is the one that is accountable in respect to the transaction/ process.

## (Business) Information
*NL: (Bedrijfs)informatie*

Business information includes all information
required (needed) by all business processes
in unison to do their work, and that are needed by them
to be delivered or needed to deliver to other business processes.
This includes the information delivered to or retrieved from
external parties.

Examples of information are from a central governmental database,
or reporting needed to be send internally or externally.

In the Pronto analysis the central point is that 
all information needs of all the business processes need to be fulfilled.
It needs to be made explicit and un-ambiguous what is the source of the information. The information source and the information need are internal or external.

## Information Flow
*NL: Informatiestroom*

Every information flow is represented by an "arrow" 
from the information object to the main activity.

When the arrow is incoming in perspective of the main activity, 
This represents the information needed for the business process.
```mermaid
graph LR;
    A[Information Object]-->B[Main Activity];
```

When the arrow is incoming in perspective of the information object,
This represents the information needed for another business process.
```mermaid
graph RL;
    A[Main Activity]-->B[Information Object];
```

An information flow entails the identification of specific information, a
and not generic information objects.
For example address-information and not customer-information.

See also Information service.


## Information Object
*NL: Informatieobject*

An Information Object is a collection of business information 
that from a logical point of view should be grouped together,
For example all information regarding customers,
*and* that share the same and only owner. 
It is often the case that the owner is the domain which
provides the information or stores the information.

All the "incoming" information flows together define the total
information needed for the enterprise. 
The sum of all the information flows together defined the content
of the sum of all the information objects. 

For each information object the following validation applies:
1. is all information stored defined by an information need (read)
1. do all the information fields sufficiently supported by administrative processes to create, update, and delete the specific information.
And if this is not the case, this should be made explicit and accepted.


|CRUD | in English
|---- |---- 
|**C**| Create
|**R**| Read
|**U**| Update
|**D**| Delete

Create, read, update, and delete (CRUD) are the four basic operations on
data. According to 
[wikipedia](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete) 
the term was likely first popularized by James Martin 
in his 1983 book Managing the Data-base environment.

## Information Service
*NL: Informatieservice*

Information services address the translation into Information 
Technology (IT).
Information services therefore address information supply and not information need. 
Pronto addresses information needs and therefore does not address information services.

Information services are the result of the implementation of information flows.
Information services are the "IT interface" to access and use 
the information from the information objects,
enabling the create, read, update and delete operations
on specific information in the information object.

The information flows defined in the Pronto Models are a 
good starting point to elicitation the IT-Requirements.


