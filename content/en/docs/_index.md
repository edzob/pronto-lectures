---
title: "Documentation"
linkTitle: "Documentation"
weight: 1
menu:
  main:
    weight: 10
---

Welcome to the Pronto Lectures.
On this page you will find documentation on what Pronto is
and the origin story. 
Eventually this is also the place for mini-lectures by
the founding fathers of pronto.

## Business Essence
1. Pronto focuses on the essence of an organization.
1. The essence are the transactions between people, who are accountable.
1. Not part of the essence of an organization are the implementation 
in the form of roles, delegation, (business) process definition etc.