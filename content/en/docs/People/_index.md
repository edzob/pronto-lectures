---
title: "People relevant to Pronto and Pronto Lectures"
linkTitle: "People"
weight: 20
description: >
  Who played a role in the creating of Pronto
---

# Pronto Founders
Bert Noorman is to be considered the founder and 
patriarch of Pronto. 
Bert worked closely together with Adosh van der Heijden
and Michael Oosterhout in the Pronto core-competence group
to extend and improve Pronto based on their experiences
and discussions. 
See the History page for more details on this.

| Name                  | Current / Profile   | Discription  
|-------------------    |-----------------    |----------------- 
| Bert Noorman          | ...                 | Founder of Pronto   
| Adosh van der Heijden  | ...                 | ...   
| Michael Oosterhout    | ...                 | ...          

# Pronto Core-team Leadership
In the period of 2006 and 2011 Pronto was
actively supported by the business development within 
Sogeti Netherlands. 
In the periode after 2011 the mantel to keep the legacy
alife was passed on within the community of professionals.

| Name                  | Current / Profile   | Discription  
|-------------------    |-----------------    |----------------- 
| Pieter Veefkind       | ...                 | ...   


# Pronto Lectures scribes
Edzo Botjes and Ton Euterbrock vollunteerd to
write down the pronto knowledge into a website.
Until today the knowledge on Pronto primary exists in the heads, minds
and memory of the original founds of Pronto.
The goal is that professionals, fans and academics can find the
knowledge and experience on Pronto via the internet. 

To write down and conserve the knowledge for future generations.

| Name                  | Current / Profile   | Discription  
|-------------------    |-----------------    |----------------- 
| Edzo Botjes           | ...                 | ...
| Ton Eusterbrock       | ...                 | ...   

