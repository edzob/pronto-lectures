# https://hub.docker.com/r/klakegg/hugo/
# from version 0.57.0 this container supports
# NodeJS PostCSS and Autoprefixer

# FROM klakegg/hugo:ext-alpine
# RUN apk add git
# RUN apk add --update npm

FROM klakegg/hugo:debian
RUN apt update
RUN apt --reinstall install npm -y

RUN npm install -g npm
RUN hugo version
RUN node -v
RUN npm -v
RUN npm install postcss postcss-cli autoprefixer --save-dev


# https://gitlab.com/gitlab-ci-utils/docker-hugo
# https://www.gohugo.org/doc/overview/usage_en/
# https://gohugo.io/commands/hugo_server/

# docker container run --rm -v ${pwd}:/site registry.gitlab.com/gitlab-ci-utils/docker-hugo:latest
# docker container run --rm -it -p 1313:1313 -v ${pwd}:/site registry.gitlab.com/gitlab-ci-utils/docker-hugo:latest hugo server --bind 0.0.0.0 --renderToDisk
# docker container run --rm -it -p 1313:1313 -v ${pwd}:/site registry.gitlab.com/gitlab-ci-utils/docker-hugo:latest sh

# sudo docker container run --rm -it -p 1313:1313 -v $(pwd):/site registry.gitlab.com/pages/hugo/hugo_extended:latest sh


